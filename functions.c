#include "main.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int getInput()
{
    char ip[10];
    int result = -1;
    int input = -1;

    fgets(ip, sizeof(ip), stdin);
    result = sscanf(ip, "%d", &input);
    if(result > 0 && input >= 0) //Usable only with positive result since it doesn't handle -1
    {
        return input;
    }
    return -1;
}

//Draw top menu
void drawTop(int size)
{
    int i = 0;
    char brick = 35;
    char topMenu[size-1];

    for(i=0; i<size-3; i++)
    {
        topMenu[i] = brick;
    }
    topMenu[i] = 0x00;
    printf("\n┏%s┓", topMenu);

}

//Draw bottom menu
void drawBot(int size)
{
    int i = 0;
    char brick = 35;
    char botMenu[size-2];
    for(i=0; i<size-3; i++)
    {
        botMenu[i] = brick;
    }
    botMenu[i] = 0x00;
    printf("\n┗%s┛\n", botMenu);

}

// Draw one menu row
void drawRow(char* text, int size)
{
    int i = 0;
    int len = 0;
    char* blankSpace = NULL;
    char space = 32;

    len = strlen(text);
    blankSpace = malloc(sizeof(char)*(size+1));

    for(i = 0;  i < (size - (len+4)); i++)
    {
        blankSpace[i] = space;
    }
    blankSpace[i] = 0x00;
    printf("\n┃%c%s%s┃", space, text, blankSpace);
    free(blankSpace);
}
