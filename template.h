#ifndef TEMPLATE_H_INCLUDED
#define TEMPLATE_H_INCLUDED

int* fetchTemplateSize(FILE* templateFile);
char** fetchTemplate(FILE* templateFile, int* sizes);
char** enlarge(char** board, int* sizes, int enlargement);

#endif // TEMPLATE_H_INCLUDED
