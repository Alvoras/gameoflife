#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

void draw(char** board, int v, int h);
int checkState(char** board, int h, int v, int sizeH, int sizeV);
char** classicRules(char** board, int v, int h);
void freeBiDimBoard(char** board, int v);

#endif // MAIN_H_INCLUDED
