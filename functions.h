// --- PROTOTYPES ---

int getInput();
int getRowSize(char** array, int size);
void drawTop(int rowSize);
void drawBot(int rowSize);
void drawRow(char* text, int rowSize);
