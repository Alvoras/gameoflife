#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int* fetchTemplateSize(FILE* templateFile){
  int* rtn = NULL;
  size_t len = 0;
  char* buffer = NULL;

  rtn = malloc(sizeof(int)*2);

  for (size_t i = 0; i < 2; i++) {
    getline(&buffer, &len, templateFile);
    sscanf(buffer, "%d", &rtn[i]);
  }

  return rtn;
}
// ### --- RESUME WORK HERE --- ###
// ENLARGE HS
char** enlarge(char** board, int* sizes, int enlargement){
  char** largerBoard = NULL;
  int newVSize = sizes[0]+enlargement*2; // 3 TOP AND 3 BOTTOM
  int newHSize = sizes[1]+enlargement*2; // 3 LEFT AND 3 RIGHT
  int i, j = 0;


  // MALLOC HOSTING BOARD AND FILL IT WITH BLANK SPACES (CHARCODE 32)
  largerBoard = malloc(sizeof(char*)*newVSize);
  for (i = 0; i < newHSize; i++) {
    largerBoard[i] = malloc(sizeof(char)*newHSize);
  }

  for (i = 0; i < newVSize; i++) {
    for (j = 0; j < newHSize; j++) {
      if ( (i < enlargement ||i >= newVSize-enlargement) ) { // IF I IS INSIDE THE PREVIOUS BOARD
        largerBoard[i][j] = 32;
      }else{
        largerBoard[i][j] = board[i-enlargement][j-enlargement];
      }

      if (j < enlargement ||j >= newHSize-enlargement) {
        largerBoard[i][j] = 32;
      }
    }
  }

  return largerBoard;
}
char** fetchTemplate(FILE* templateFile, int* sizes){

  char liveCell = 48;
  char deadCell = 32;

  size_t len = 0;
  int strLength, cnt, i, j, k = 0;
  char* buffer = NULL;
  char** board = NULL;

  board = malloc(sizeof(char*)*sizes[0]);
  for (i = 0; i < sizes[0]; i++) {
    board[i] = malloc(sizeof(char)*sizes[1]);
  }
  i = 0;

  while ( (cnt = getline(&buffer, &len, templateFile)) != -1) {
    strLength = strlen(buffer);
    for (j = 0; j < strLength; j++) {
      if ( (isdigit(buffer[j])) ) {
        if (buffer[j] == 48) {
          buffer[j] = deadCell;
        }else if (buffer[j] == 49) {
          buffer[j] = liveCell;
        }
        board[i][k] = buffer[j];
        k++;
      }
    }
    k = 0;
    i++;
  }

  return board;
}
