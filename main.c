#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "template.h"
#include "gameoflife.h"

// --- MAIN ---
int main(int argc, char* argv[]) {
   FILE* templateFile = NULL;
   char** board = NULL;
   char** tempBoard = NULL;
   char* templatePath = NULL;
   char* ruleset = NULL;
   unsigned short cnt = 0;
   long int sleepingTime = 80000; // DEFAULT 25 IPS AT 40 000
   int paramCnt = 0;
   int paramDebug = 0;
   int opt;
   int chosenRuleset = 0;
   int enlargement = 3;

   int* sizes = NULL;
   sizes = malloc(sizeof(int*)*2);

   if( !(argc > 1) ) printf("Usage : %s [-t <template path>][-r <ruleset>][-cd]\n", argv[0]), exit(EXIT_FAILURE);

   while ( (opt = getopt(argc, argv, "cdt:r:")) != -1 ) {
     switch (opt) {
       case 'c':
         paramCnt = 1;
         break;
       case 'd':
         paramDebug = 1;
         break;
       case 't':
         templatePath = optarg;
         break;
       case 'r':
         ruleset = optarg;
         break;
       default:
         printf("Usage : %s [-t <template path>][-r <ruleset>][-cd]\n", argv[0]);
         exit(EXIT_FAILURE);
         break;
     }
   }

  if ( (templateFile = fopen(templatePath, "r")) ) {
     printf("Loading %s...\n", templatePath);
     sizes = fetchTemplateSize(templateFile);
     if (sizes[0] < 100 && sizes[1] < 100) {
       board = fetchTemplate(templateFile, sizes);

       //--- DRAW LOOP

            if (ruleset == NULL ||strcmp(ruleset, "classic") == 0) {
              chosenRuleset = 1;
            }else if (strcmp(ruleset, "highlife") == 0) {
              chosenRuleset = 2;
              system("clear");
              printf("RULESET IMPLEMENTATION PENDING !\n\nExiting...\n");
              return 0;
            }else if (strcmp(ruleset, "dev") == 0) {
              chosenRuleset = 3;
            }else{
              printf("Unrecognized ruleset\n");
              return 0;
            }

            board = enlarge(board, sizes, enlargement);
            sizes[0] += enlargement*2;
            sizes[1] += enlargement*2;
            do {
             draw(board, sizes[0], sizes[1]);

             if ( (paramCnt) ) {
               printf("\n%d\n", cnt);
             }

             switch (chosenRuleset) {
              case 1:
                tempBoard = classicRules(board, sizes[0], sizes[1]);
                break;
              case 2:
                // HIGHLIFE RULESET WIP
                break;
              default:
                printf("Unrecognized ruleset\n");
                return 0;
              break;
             }

             for (size_t i = 0; i < sizes[0]; i++) {
               for (size_t j = 0; j < sizes[1]; j++) {
                 board[i][j] = tempBoard[i][j];
               }
             }
             cnt++;
             if ( (paramDebug) ) {
               getchar(); // DEBUG
             }
             freeBiDimBoard(tempBoard, sizes[0]);
             usleep(sleepingTime);
          } while(1);
     }else{
       printf("ERROR : template too large\n");
       return 0;
     }

     fclose(templateFile);
     freeBiDimBoard(board, sizes[0]);
     free(sizes);
   }else{
     printf("ERROR : File not found\n");
     return 0;
   }

   return 0;
}
