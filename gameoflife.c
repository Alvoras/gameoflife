#include <stdlib.h>
#include <stdio.h>

char liveCell = 48;
char deadCell = 32;

int checkState(char** board, int h, int v, int sizeH, int sizeV){
   unsigned short cnt = 0;
    // IF H == 0 THEN NO CHECK UP
    // IF V == 0 THEN NO CHECK LEFT
    // IF H == SIZE THEN NO CHECK BOTTOM
    // IF V == SIZE THEN NO CHECK RIGHT

   for (int i = -1; i < 2; i++) {
     for (int j = -1; j < 2; j++) {
       if (!(i == 0 && j == 0)) { // TO PREVENT A CELL TO EVALUATE HERSELF

         if (!(h == 0 && i == -1) && !(h == sizeH-1 && i == 1) && !(v == 0 && i == -1) && !(v == sizeV-1 && i == 1)) { // TO PREVENT THE CHECK OF A NON EXISTENT CELL
          // printf("cell checked :%d, %d, %d, %d\n", h, v, i, j);
           if (board[h+i][v+j] == liveCell) {
             cnt++;
           }
         }else if ( board[h][v] == liveCell ) { // IF WE ARE ON A BORDER AND THE CURRENT CELL IS ALIVE, RETURN BORDER CODE : -1
           return -1;
         }

       }
     }
   }

   return cnt;
}
char** classicRules(char** board, int v, int h){
  char** newBoard = NULL;
  unsigned short i = 0;
  unsigned short j = 0;
  unsigned short neighbour = 0;

  // MALLOC NEW BOARD
  newBoard = malloc(sizeof(char*)*v);
  for (i = 0; i < v; i++) {
     newBoard[i] = malloc(sizeof(char)*h);
  }

  // EVAL AND IMPORT NEW BOARD
  for (i = 0; i < v; i++) {
    for (j = 0; j < h; j++) {
      neighbour = checkState(board, i, j, v, h);

      if (neighbour == -1) { // IF THE CHECKSTATE FUNCTION HAS DETECTED A BORDER, DELETE CURRENT CELL
        newBoard[i][j] = deadCell;
        return newBoard;
      }

      if(board[i][j] == deadCell && neighbour == 3){
        newBoard[i][j] = liveCell;
      }else if(board[i][j] == liveCell && !(neighbour == 2 || neighbour == 3)){
        newBoard[i][j] = deadCell;
      }else{
        newBoard[i][j] = board[i][j];
      }
    }
  }

  return newBoard;
}
void freeBiDimBoard(char** board, int v){
  for (size_t i = 0; i < v; i++) {
    free(board[i]);
  }

  free(board);
}
void draw(char** board, int v, int h) {
   system("clear");

   for (size_t i = 0; i < v; i++) {
      for (size_t j = 0; j < h; j++) {
         printf("%c ", board[i][j]);
         if (j == h-1) {
            printf("\n");
         }
      }
   }
}
