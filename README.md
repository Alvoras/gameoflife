# Conway's Game of Life

The Game of Life is a cellular automaton created by John Conway in 1970. Start with a template and watch it unfold !

Usage : gameoflife [-t \<template path\>][-r \<ruleset\>][-cd]

Template avalaible :
   - The glider gun

Ruleset avalaible :
   - Classic

Options avalaible :  
   - c Display a frame counter  
   - d Press a key to display the next frame  
